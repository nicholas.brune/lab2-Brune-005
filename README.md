number 1. 
120/4 = 30
4 common pins + 30 = 34 pins are required to drive the LCD. 

number 2. 
the GPIO pin cannot perform all functions listed in figure 2. 

number 3. 
Yes, the LCD is built in within the processor chip. the controller generates 
signals to the common terminals (COM) and segment lines (SEG) to drive the 
external LCD.

number 4.
the processor LCD driver can drive 24 pixels at a time.
their are 96 bits for the LCD_RAM. 

number 5. 
their are 256 pixels installed on the discovery kit . many LCD_RAM registers do not
use the odd LCD_RAM because they are not needed. 